import cython

cpdef fib(int n):
    cdef int total = 0
    cdef int i = 0
    for i in range(n+1):
        total += i
    return total

