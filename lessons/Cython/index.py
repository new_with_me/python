import Cython.fib as fib
import time

start = time.time()
print(fib.fib(9999))
stop = time.time()
res1 = stop - start


def myfun(n):
    total = 0
    for i in range(n + 1):
        total += i
    return total


start = time.time()
print(myfun(9999))
stop = time.time()
res2 = stop - start
print(res1, res2)
print(res1<res2, res2-res1)
print(fib.fib(9999) == myfun(9999))