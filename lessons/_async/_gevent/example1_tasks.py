import gevent


def task(task_id):
    print('Gevent sleep', task_id)
    gevent.sleep(1)
    print('Gevent finished', task_id)


# помещаем функцию в гринлеты
jobs = [
    gevent.spawn(task(1)),
    gevent.spawn(task(2)),
    gevent.spawn(task(3))
]

# блокируем дальнейшую работу программы и дожидаемся выполнения всех гринлетов
gevent.joinall(jobs)
