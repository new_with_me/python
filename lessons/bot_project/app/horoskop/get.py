from selenium import webdriver
from selenium.webdriver.common.by import By


zodiacs = {
    'Овен': 'oven',
    'Телец': 'telec',
    'Близнецы': 'bliznecy',
    'Рак': 'rak',
    'Лев': 'lev',
    'Дева': 'deva',
    'Весы': 'vesy',
    'Скорпион': 'scorpion',
    'Стрелец': 'strelec',
    'Козерог': 'kozerog',
    'Водолей': 'vodolei',
    'Рыбы': 'ryby'
}

terms = {
    "Сегодня": "goroskop_na_segodnya",
    "Завтра": "goroskop_na_zavtra", "Неделя": "goroskop_na_nedelyu",
}

def get_zodiac(term: str, zodiac: str) -> str:
    try:
        options = webdriver.EdgeOptions()
        options.add_argument("--headless")
        browser = webdriver.Edge()
        browser.get(f"https://www.oculus.com.ua/{terms[term]}/{zodiacs[zodiac]}.html")
        get_text = browser.find_element(By.CLASS_NAME, "oculus-epz-text")
        result = get_text.text
        browser.close()
    except Exception:
        result = "Гороскоп еще не готов"
    return result
    

