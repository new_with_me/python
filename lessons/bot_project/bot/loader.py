from aiogram import Bot, Dispatcher
from aiogram.enums import ParseMode
# import token
from config.config import TOKEN

bot = Bot(token=TOKEN, parse_mode=ParseMode.HTML)
dp = Dispatcher()
