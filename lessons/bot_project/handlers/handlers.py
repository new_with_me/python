from random import randint

from aiogram.filters import CommandStart, Command
from aiogram import types, Bot, F
from aiogram.utils.markdown import hbold
from aiogram.types import Message
from aiogram.utils.keyboard import InlineKeyboardBuilder

from goroskop import znak
from loader import dp


# @dp.message(CommandStart())
# async def command_start_handler(message: types.Message) -> None:
#     await message.answer(f'Hello, {hbold(message.from_user.full_name)}!')
# @dp.message(Command("start"))
# async def command_start_handler(message: types.Message):
#     kb = [
#         [types.KeyboardButton(text="Робот")],
#         [types.KeyboardButton(text="Не робот")],
#         [types.KeyboardButton(text="Робот, но мне сказали я не робот")],
#          ]
#     keyboard = types.ReplyKeyboardMarkup(keyboard=kb, resize_keyboard=True, input_field_placeholder="Ты робот?")
#     keyboard.row_width = 2
#
#     await message.answer("Привет, ты кто?", reply_markup=keyboard)
#
# @dp.message()
# async def echo_handler(message: types.Message) -> None:
#     try:
#         await message.answer(f"you said: {message.text}")
#     except TypeError:
#         await message.answer('Nice try!')


@dp.message(Command("inline"))
async def cmd_inline_url(message: types.Message, bot: Bot):
    builder = InlineKeyboardBuilder()
    builder.row(types.InlineKeyboardButton(
        text="GitHub", url="https://github.com")
    )
    builder.row(types.InlineKeyboardButton(
        text="Оф. канал Telegram",
        url="tg://resolve?domain=telegram")
    )
    user_id = 5495357114
    chat_info = await bot.get_chat(user_id)
    if not chat_info.has_private_forwards:
        builder.row(
            types.InlineKeyboardButton(
                text="Какой-то пользователь",
                url=f"tg://user?id={user_id}")
        )
    await message.answer(
        'Выберите ссылку', reply_markup=builder.as_markup(),
    )


@dp.message(Command("random"))
async def cmd_random(message: types.Message):
    builder = InlineKeyboardBuilder()
    builder.add(types.InlineKeyboardButton(
        text="Нажми меня",
        callback_data="random_value")
    )
    await message.answer(
        "Нажмите на кнопку, чтобы бот отправил число от 1 до 10",
        reply_markup=builder.as_markup()
    )


@dp.callback_query(F.data == "random_value")
async def send_random_value(callback: types.CallbackQuery):
    await callback.message.answer(str(randint(1, 10)))
    await callback.answer()


@dp.message(Command("goroskop"))
async def cmd_random(message: types.Message):
    builder = InlineKeyboardBuilder()
    builder.add(types.InlineKeyboardButton(
        text="Овен",
        callback_data="Овен")
    )
    builder.add(types.InlineKeyboardButton(
        text="Телец",
        callback_data="Телец")
    )
    builder.add(types.InlineKeyboardButton(
        text="Близнецы",
        callback_data="Близнецы")
    )
    builder.add(types.InlineKeyboardButton(
        text="Рак",
        callback_data="Рак")
    )
    builder.add(types.InlineKeyboardButton(
        text="Лев",
        callback_data="Лев")
    )
    builder.add(types.InlineKeyboardButton(
        text="Дева",
        callback_data="Дева")
    )
    builder.add(types.InlineKeyboardButton(
        text="Весы",
        callback_data="Весы")
    )
    builder.add(types.InlineKeyboardButton(
        text="Скорпион",
        callback_data="Скорпион")
    )
    builder.add(types.InlineKeyboardButton(
        text="Стрелец",
        callback_data="Стрелец")
    )
    builder.add(types.InlineKeyboardButton(
        text="Козерог",
        callback_data="Козерог")
    )
    builder.add(types.InlineKeyboardButton(
        text="Водолей",
        callback_data="Водолей")
    )
    builder.add(types.InlineKeyboardButton(
        text="Рыбы",
        callback_data="Рыбы")
    )
    builder.adjust(3)
    await message.answer(
        "Нажмите на кнопку, чтобы бот отправил число от 1 до 10",
        reply_markup=builder.as_markup()
    )


@dp.callback_query(F.data == "Овен")
async def send_oven(callback: types.CallbackQuery):
    await callback.message.answer(znak("Овен"))
    await callback.answer()

@dp.callback_query(F.data == "Телец")
async def send_telec(callback: types.CallbackQuery):
    await callback.message.answer(znak("Телец"))
    await callback.answer()


@dp.callback_query(F.data == "Близнецы")
async def send_bliznec(callback: types.CallbackQuery):
    await callback.message.answer(znak("Близнецы"))
    await callback.answer()

@dp.callback_query(F.data == "Рак")
async def send_rak(callback: types.CallbackQuery):
    await callback.message.answer(znak("Рак"))
    await callback.answer()


@dp.callback_query(F.data == "Лев")
async def send_lev(callback: types.CallbackQuery):
    await callback.message.answer(znak("Лев"))
    await callback.answer()


@dp.callback_query(F.data == "Дева")
async def send_dev(callback: types.CallbackQuery):
    await callback.message.answer(znak("Дева"))
    await callback.answer()


@dp.callback_query(F.data == "Весы")
async def send_ves(callback: types.CallbackQuery):
    await callback.message.answer(znak("Весы"))
    await callback.answer()

@dp.callback_query(F.data == "Скорпион")
async def send_scorp(callback: types.CallbackQuery):
    await callback.message.answer(znak("Скорпион"))
    await callback.answer()


@dp.callback_query(F.data == "Стрелец")
async def send_strel(callback: types.CallbackQuery):
    await callback.message.answer(znak("Стрелец"))
    await callback.answer()

@dp.callback_query(F.data == "Козерог")
async def send_koz(callback: types.CallbackQuery):
    await callback.message.answer(znak("Козерог"))
    await callback.answer()

@dp.callback_query(F.data == "Водолей")
async def send_vod(callback: types.CallbackQuery):
    await callback.message.answer(znak("Водолей"))
    await callback.answer()

@dp.callback_query(F.data == "Рыбы")
async def send_ryb(callback: types.CallbackQuery):
    await callback.message.answer(znak("Рыбы"))
    await callback.answer()
