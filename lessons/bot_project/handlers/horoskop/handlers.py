

from aiogram.filters import CommandStart, Command
from aiogram import types, Bot, F, Router
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import StatesGroup, State
from aiogram.types import InlineKeyboardMarkup
from aiogram.utils.keyboard import InlineKeyboardBuilder

from handlers.horoskop.horoskop import znak

horoscope = Router()

terms = ["Сегодня", "Завтра", "Неделя"]
zodiacs = ['Овен', 'Телец', 'Близнецы', 'Рак', 'Лев', 'Дева', 'Весы',
    'Скорпион', 'Стрелец', 'Козерог', 'Водолей', 'Рыбы']

def make_row_keyboard(items: list[str]) -> InlineKeyboardMarkup:
    row = [types.InlineKeyboardButton(text=item) for item in items]
    return InlineKeyboardMarkup(keyboard=[row], resize_keyboard=True)


class ZodiacRobot(StatesGroup):
    terms = State()
    zodiacs = State()

@horoscope.message(Command("start"))
async def cmd_random(message: types.Message, state: FSMContext):
    builder = InlineKeyboardBuilder()
    [builder.add(types.InlineKeyboardButton(
        text=i,
        callback_data=i)
    ) for i in zodiacs]
    builder.adjust(3)
    await message.answer(
        "Выберите знак зодиака",
        reply_markup=builder.as_markup()
    )
    await state.set_state(ZodiacRobot.zodiacs)



@horoscope.callback_query(F.data.in_(zodiacs))
async def process_zodiac(callback: types.CallbackQuery, state: FSMContext):
    await state.update_data(zodiac=callback.data)  # Обновляем данные состояния
    term = InlineKeyboardBuilder()
    [term.add(types.InlineKeyboardButton(
        text=i,
        callback_data=i)
    ) for i in terms]
    await callback.message.answer(
        "Теперь выберите время прогноза",
        reply_markup=term.as_markup()
    )
    await state.set_state(ZodiacRobot.terms)

@horoscope.callback_query(F.data.in_(terms))
async def send_(callback: types.CallbackQuery, state: FSMContext):
    await state.update_data(terms=callback.data)
    data = await state.get_data()  # Получаем данные состояний
    zodiac = data.get('zodiac')     # Получаем значение состояния знака зодиака
    term = data.get('terms')
    print(zodiac, term)# Получаем значение состояния времени прогноза
    await callback.message.answer(znak(zodiac, term))  # Вызываем функцию znak() с передачей двух состояний
    await callback.message.answer("/start")

    
@horoscope.callback_query(F.data == "Телец")
async def send_telec(callback: types.CallbackQuery):
    await callback.message.answer(znak("Телец"))
    await callback.message.answer("/start")


@horoscope.callback_query(F.data == "Близнецы")
async def send_bliznec(callback: types.CallbackQuery):
    await callback.message.answer(znak("Близнецы"))
    await callback.message.answer("/start")


@horoscope.callback_query(F.data == "Рак")
async def send_rak(callback: types.CallbackQuery):
    await callback.message.answer(znak("Рак"))
    await callback.message.answer("/start")


@horoscope.callback_query(F.data == "Лев")
async def send_lev(callback: types.CallbackQuery):
    await callback.message.answer(znak("Лев"))
    await callback.message.answer("/start")


@horoscope.callback_query(F.data == "Дева")
async def send_dev(callback: types.CallbackQuery):
    await callback.message.answer(znak("Дева"))
    await callback.message.answer("/start")


@horoscope.callback_query(F.data == "Весы")
async def send_ves(callback: types.CallbackQuery):
    await callback.message.answer(znak("Весы"))
    await callback.message.answer("/start")


@horoscope.callback_query(F.data == "Скорпион")
async def send_scorp(callback: types.CallbackQuery):
    await callback.message.answer(znak("Скорпион"))
    await callback.message.answer("/start")


@horoscope.callback_query(F.data == "Стрелец")
async def send_strel(callback: types.CallbackQuery):
    await callback.message.answer(znak("Стрелец"))
    await callback.message.answer("/start")


@horoscope.callback_query(F.data == "Козерог")
async def send_koz(callback: types.CallbackQuery):
    await callback.message.answer(znak("Козерог"))
    await callback.message.answer("/start")


@horoscope.callback_query(F.data == "Водолей")
async def send_vod(callback: types.CallbackQuery):
    await callback.message.answer(znak("Водолей"))
    await callback.message.answer("/start")


@horoscope.callback_query(F.data == "Рыбы")
async def send_ryb(callback: types.CallbackQuery):
    await callback.message.answer(znak("Рыбы"))
    await callback.message.answer("/start")


