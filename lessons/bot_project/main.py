import asyncio
import logging
import sys

from aiogram import Router

from bot.loader import dp, bot
# from handlers.horoskop.handlers import horoscope
from handlers.routers import lists
# from routers.routers import lists

router = Router()


dp.include_routers(lists)

async def main() -> None:
    await dp.start_polling(bot)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    asyncio.run(main())
