import asyncio
import logging
import os
import sys

from aiogram import types, Router, F, Bot, Dispatcher
from aiogram.enums import ParseMode
from aiogram.filters import CommandStart, Command, StateFilter
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import StatesGroup, State
from aiogram.types import Message, ReplyKeyboardRemove, KeyboardButton, ReplyKeyboardMarkup
from aiogram.utils.markdown import hbold
from dotenv import load_dotenv

load_dotenv()

TOKEN = str(os.getenv('BOT_TOKEN'))
bot = Bot(token=TOKEN, parse_mode=ParseMode.HTML)
dp = Dispatcher()
router_start = Router()
dp.include_router(router_start)
# Константы для конфигурации робота
ROBOT_COLORS = ["Красный", "Синий", "Зеленый"]
ROBOT_FUNCTIONS = ["Уборка", "Компаньон", "Безопасность"]

class OrderRobot(StatesGroup):
    choosing_color = State()
    choosing_function = State()

def make_row_keyboard(items: list[str]) -> ReplyKeyboardMarkup:
    """
    Создаёт реплай-клавиатуру с кнопками в один ряд
    :param items: список текстов для кнопок
    :return: объект реплай-клавиатуры
    """
    row = [KeyboardButton(text=item) for item in items]
    return ReplyKeyboardMarkup(keyboard=[row], resize_keyboard=True)

@router_start.message(Command("start"))
async def command_start_handler(message: Message):
    await message.answer(f"Hello, {hbold(message.from_user.full_name)}!")
    await message.answer("Привет, для выбора товара или услуги запусти команду:\n"
                         "\n"
                         "/robot")

@router_start.message(Command("robot"))
async def cmd_robot(message: Message, state: FSMContext):
    await message.answer(
        text="Добро пожаловать в мир роботов! Выберите цвет своего будущего робота:",
        reply_markup=make_row_keyboard(ROBOT_COLORS)
    )
    await state.set_state(OrderRobot.choosing_color)

@router_start.message(OrderRobot.choosing_color, F.text.in_(ROBOT_COLORS))
async def color_chosen(message: Message, state: FSMContext):
    await state.update_data(chosen_color=message.text.lower())
    await message.answer(
        text="Отлично! Теперь выберите функцию для вашего робота:",
        reply_markup=make_row_keyboard(ROBOT_FUNCTIONS)
    )
    await state.set_state(OrderRobot.choosing_function)

@router_start.message(OrderRobot.choosing_function, F.text.in_(ROBOT_FUNCTIONS))
async def function_chosen(message: Message, state: FSMContext):
    user_data = await state.get_data()
    await message.answer(
        text=f"Вы выбрали робота цвета {user_data['chosen_color']}.\n"
             f"Функция: {message.text.lower()}.\n"
             f"Теперь можете перейти к выбору аксессуаров: /accessories",
        reply_markup=ReplyKeyboardRemove()
    )
    await state.clear()

async def main() -> None:

    bot = Bot(token=TOKEN, parse_mode=ParseMode.HTML)

    await dp.start_polling(bot)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    asyncio.run(main())