from functools import lru_cache


def get_fib_new(n):
    if n < 2:
        return n
    return get_fib(n - 1) + get_fib(n - 2)


@lru_cache(maxsize=100)
def get_fib(n):
    if n < 2:
        return n
    return get_fib(n - 1) + get_fib(n - 2)


result = get_fib(10)
print(result)
# Получить информацию о кэше
print(get_fib.cache_info())
