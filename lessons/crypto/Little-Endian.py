# Little-Endian
a = 2
b = "0000010000000000000000000000"
c = "1111111 1111111 1111110 1101001"


def en(n: int) -> str:
    m = str(bin(n))[2:]
    m = "0" * (28 - len(m))+m
    a1 = m[:7]
    a2 = m[7:14]
    a3 = m[14:21]
    a4 = m[21:28]
    return a4 + a3 + a2 + a1


m = en(a)

print(m)
print(b == m)