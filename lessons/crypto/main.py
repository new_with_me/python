import hashlib

import base58
import bitcoin

a = "1Di3yvJduQHXrJJQjbSMaSE4gERYC6Zhdk"

import bitcoin
def binary_to_base58(binary_address):
    # Преобразование двоичного представления в шестнадцатеричное
    hex_address = bitcoin.hex_to_b58check(int(binary_address, 2))[2:]

    # Применение двойного SHA-256 хэша
    hash1 = hashlib.sha256(bytes.fromhex(hex_address)).digest()
    hash2 = hashlib.sha256(hash1).digest()

    # Добавление первых 4 байт хэша в конец шестнадцатеричной строки
    checksum = hash2[:4]
    hex_address += checksum.hex()

    # Преобразование обратно в бинарное представление
    binary_address_with_checksum = bytes.fromhex(hex_address)

    # Преобразование в Base58
    base58_address = base58.b58encode(binary_address_with_checksum)

    return base58_address

with open("dataset.txt", "r") as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content]
f.close()
print(content)

for i in content:
    print(i)
    print(bitcoin.sha256(i))
    print('\n')

