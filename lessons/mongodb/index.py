import time

from pymongo.mongo_client import MongoClient
from pymongo.server_api import ServerApi
from sys import getsizeof

class Mongo:
    def __init__(self, uri):
        self.uri = uri
        self.client = MongoClient(uri, server_api=ServerApi('1'))

    def is_collections(self, col_name="*") -> list:
        if col_name == "*":
            return self.client.list_database_names()
        is_db = self.client[col_name].list_collection_names()
        if len(is_db) == 0:
            return ["Collection not found"]
        return is_db

    def insert(self, base_name: str, collection: str, data: dict):
        self.client[base_name][collection].insert_one(data)

    def find(self, base_name: str, collection: str, data: dict):
        return self.client[base_name][collection].find_one(data)

    def update(self, base_name: str, collection: str, data: dict):
        self.client[base_name][collection].update_one(data)

    def delete(self, base_name: str, collection: str, data: dict):
        self.client[base_name][collection].delete_one(data)

    def drop(self, base_name: str, collection: str):
        self.client[base_name][collection].drop()

    def get_all(self, base_name: str, collection: str):
        return self.client[base_name][collection].find()

    def close(self):
        self.client.close()
start = time.time()
obj = Mongo("mongodb+srv://go280286sai:Ldflwfnmd0ctvm@cluster0.zziuq9q.mongodb.net/?retryWrites=true&w=majority")
db = obj.is_collections()
# obj.insert("testdate", "users", {"name": "Aleksey", "last_name": "Coop", "age": 30})
# find = obj.find("testdate", "users", {"name": "Aleksey"})
find = obj.get_all("testdate", "users")
print(getsizeof(find))
for find in find:
    print(find)
print(find)
stop = time.time()
print(stop - start, "seconds")


