import multiprocessing
import time


def run_proc():
    print('Run child process %s (%s)' % (multiprocessing.current_process().name, multiprocessing.current_process().pid))


process = (multiprocessing.Process(target=run_proc))
process.start()
process.join()

for _ in range(3):
    time.sleep(1)
    multiprocessing.Process(target=run_proc).start()
