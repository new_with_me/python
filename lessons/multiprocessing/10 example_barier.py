import multiprocessing
from multiprocessing import Process, Barrier


#Минимальное количество процессоров для запуска
bar = Barrier(3)

def f1(bar):
    print(multiprocessing.current_process().pid)
    bar.wait()
    print(multiprocessing.current_process().pid, "is started")

for i in range(3):
    p = Process(target=f1, args=(bar,))
    p.start()