from multiprocessing import Process, Manager


def f(m_dict, m_arr):
    m_dict["name"] = "test"
    m_dict["version"] = "1.0"
    m_arr.append(1)
    m_arr.append(2)


if __name__ == '__main__':
    with Manager() as manager:
        m_dict = manager.dict()
        m_arr = manager.list()
        p = Process(target=f, args=(m_dict, m_arr))
        p.start()
        p.join()
        print("m_dict", m_dict)
        print("m_arr", m_arr)

