from multiprocessing.managers import BaseManager
import time


def get_time():
    return time.time()


BaseManager.register('get', get_time)
manager = BaseManager(address=('192.168.0.107', 5000), authkey=b'abc')
server = manager.get_server()
print("server started")
server.serve_forever()