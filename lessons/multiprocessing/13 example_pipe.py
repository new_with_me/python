from multiprocessing import Pipe, Process
import time


def send(pipe, data):
    pipe.send(data)
    pipe.close()


if __name__ == '__main__':
    output_c, input_c = Pipe()
    p = Process(target=send, args=(input_c, 'Hello'))
    p.start()
    print(output_c.recv())
