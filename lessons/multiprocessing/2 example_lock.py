import multiprocessing

lock = multiprocessing.Lock()


def get_value(l):
    """Разблокировать может любой процессор"""
    l.acquire()
    print('Value: ', multiprocessing.current_process().pid)
    l.release()


for _ in range(3):
    multiprocessing.Process(target=get_value, args=(lock,)).start()
