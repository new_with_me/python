import multiprocessing
import random

arr = multiprocessing.Array('i', range(10))

lock = multiprocessing.Lock()

process = []


def get_value(l, array, index):
    with l:
        num = random.randint(0, 20)
        print('Value: ', multiprocessing.current_process().pid)
        array[index] = num
        print('Value: ', num)


for i in range(10):
    pr = multiprocessing.Process(target=get_value, args=(lock, arr, i))
    process.append(pr)
    pr.start()

for pr in process:
    pr.join()

print(list(arr))
