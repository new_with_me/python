import multiprocessing


def get_text(q):
    q.put('hello world')


queue = multiprocessing.Queue()
pr = multiprocessing.Process(target=get_text, args=(queue,))
pr.start()

print(queue.get())
pr.join()