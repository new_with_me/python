import multiprocessing


def get_value(value):
    print('Value: ', multiprocessing.current_process().pid, value)
    return value


def end_function(response):
    print('Задание завершено: ', response)


if __name__ == '__main__':
    with multiprocessing.Pool(multiprocessing.cpu_count() * 3) as pool:
        pool.map_async(get_value, (1, 2, 3, 4, 5, 6, 7, 8, 9, 10), callback=end_function)
        pool.close()
        pool.join()
