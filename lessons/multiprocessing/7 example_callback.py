import multiprocessing


def out(x):
    print("out: ", x)
    return x


def end_function(response):
    print(response)


if __name__ == '__main__':
    with multiprocessing.Pool(multiprocessing.cpu_count() * 3) as pool:
        for i in range(10):
            pool.apply_async(out, args=(i,), callback=end_function)
        pool.close()
        pool.join()
