import multiprocessing
import time

event = multiprocessing.Event()


def start_fun():
    print('start fun')
    while True:
        event.wait()
        time.sleep(1)
        print('start consumer')
        event.clear()

def continue_fun():
    while True:
        time.sleep(1)
        print('continue fun')
        event.set()
        print('continue consumer')


pr = multiprocessing.Process(target=start_fun)
pr.start()
pr1 = multiprocessing.Process(target=continue_fun)
pr1.start()

