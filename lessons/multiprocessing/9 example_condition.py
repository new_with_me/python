import multiprocessing
import time

con = multiprocessing.Condition()


def f1():
    while True:
        with con:
            con.wait()
            print('Получил событие')
            print()
            time.sleep(1)


def f2():
    for i in range(100):
        if i % 10 == 0:
            with con:
                con.notify()
            time.sleep(1)
        else:
            print("{}".format(i))
            time.sleep(1)


pr1 = multiprocessing.Process(target=f1)
pr2 = multiprocessing.Process(target=f2)
pr1.start()
pr2.start()
