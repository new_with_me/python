from multiprocessing import Process, Pipe


def send(pipe, url):
    with open(url, 'r') as f:
        pipe.send(f.read())
    pipe.close()


def write(data, url):
    with open(url, 'a') as f:
        data = str(data).upper()
        f.write(data)


if __name__ == '__main__':
    output_c, input_c = Pipe()
    p = Process(target=send, args=(input_c, 'read.txt'))
    p.start()
    write(output_c.recv(), 'write.txt')
    p.join()
