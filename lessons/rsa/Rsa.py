import base64

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
import os


class Rsa:
    path_private_key: str = os.path.dirname(__file__) + "/keys/private.pem"
    path_public_key: str = os.path.dirname(__file__) + "/keys/"
    private_key: any = None
    public_key: any = None

    def __init__(self, name: str = "public"):
        if os.path.exists(self.path_public_key + name + ".pem") and os.path.exists(self.path_private_key):
            self.public_load_key(name)
            self.private_load_key()
        else:
            self.generate_key()
            raise Exception("Keys not found")

    def generate_key(self) -> any:
        self.private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
        )
        self.private_save_key()
        self.public_save_key()

    def public_save_key(self, name: str = "public"):
        self.public_key = self.private_key.public_key()
        public_pem = self.public_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )
        with open(self.path_public_key + name + ".pem", "wb") as f:
            f.write(public_pem)

    def private_save_key(self):
        private_pem = self.private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption()
        )
        with open(self.path_private_key, "wb") as f:
            f.write(private_pem)

    def public_load_key(self, name: str = "public"):
        with open(self.path_public_key + name + ".pem", "rb") as f:
            self.public_key = serialization.load_pem_public_key(
                f.read()
            )

    def private_load_key(self):
        with open(self.path_private_key, "rb") as f:
            self.private_key = serialization.load_pem_private_key(
                f.read(),
                password=None,
            )

    def encrypt(self, text_to_encrypt: str) -> bytes:
        encrypt = self.public_key.encrypt(
            text_to_encrypt.encode("utf-8"),
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )
        return base64.b64encode(encrypt)

    def decrypt(self, text_to_decrypt: bytes) -> str:
        text_to_decrypt = base64.b64decode(text_to_decrypt)
        decrypt = self.private_key.decrypt(
            text_to_decrypt,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )
        return decrypt.decode("utf-8")

