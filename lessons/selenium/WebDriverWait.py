import math
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common import NoAlertPresentException
from selenium.webdriver import Keys, ActionChains
from selenium import webdriver
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By

option = webdriver.ChromeOptions()
# option.add_argument("--headless")
browser = webdriver.Chrome(options=option)
url = "https://parsinger.ru/selenium/5.9/7/index.html"
browser.get(url)
# ------------------------------------------------------------
block = browser.find_elements(By.CLASS_NAME, 'container')
for i in range(1, len(block)+1):
    WebDriverWait(browser, 10).until((EC.element_located_to_be_selected((By.XPATH, f'/html/body/div/div[{i}]/div/input'))))
    b = browser.find_element(By.XPATH, f'/html/body/div/div[{i}]/button')
    b.click()
result = browser.find_element(By.ID, 'result').text
print(result)



