import math

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import os
import time

try:
    browser = webdriver.Edge()
    link = "http://suninjuly.github.io/file_input.html"
    browser.get(link)
    input1 = browser.find_element(By.NAME, "firstname")
    input1.send_keys("Ivan")
    input2 = browser.find_element(By.NAME, "lastname")
    input2.send_keys("Petrov")
    input3 = browser.find_element(By.NAME, "email")
    input3.send_keys("Smolensk")
    file = os.path.join(os.path.abspath(os.path.dirname(__file__)), "text.txt")
    input4 = browser.find_element(By.ID, "file")
    input4.send_keys(file)
    button = browser.find_element(By.CSS_SELECTOR, "button.btn")
    button.click()

    time.sleep(7)

except Exception as e:
    print("Something went wrong")
    print(e)