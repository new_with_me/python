from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

import time

try:
    link = "http://suninjuly.github.io/selects1.html"
    browser = webdriver.Edge()
    browser.get(link)

    num1 = browser.find_element(By.ID, "num1")
    num2 = browser.find_element(By.ID, "num2")
    print("num1", num1.text)
    print("num2", num2.text)
    y = int(num1.text) + int(num2.text)
    print("y: ", y)

    select = Select(browser.find_element(By.ID, "dropdown"))
    for option in select.options:
        print(option.text)
        if option.text == str(y):
            print("if: ", option.text)
            option.click()

    button = browser.find_element(By.CSS_SELECTOR, "button.btn-default")
    button.click()

    time.sleep(1)



finally:
    time.sleep(10)
    browser.quit()
