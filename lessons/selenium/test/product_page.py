# pages/product_page.py

from .base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoAlertPresentException
import math

class ProductPage(BasePage):
    ADD_TO_BASKET_BUTTON = (By.CSS_SELECTOR, "button.btn-add-to-basket")
    PRODUCT_NAME = (By.CSS_SELECTOR, "h1")
    PRODUCT_PRICE = (By.CSS_SELECTOR, "p.price_color")
    SUCCESS_MESSAGE = (By.CSS_SELECTOR, "div.alertinner")
    BASKET_TOTAL_MESSAGE = (By.CSS_SELECTOR, "div.alertinner > p strong")

    def add_product_to_basket(self):
        add_to_basket_button = self.browser.find_element(*self.ADD_TO_BASKET_BUTTON)
        add_to_basket_button.click()

    def get_product_name(self):
        return self.browser.find_element(*self.PRODUCT_NAME).text

    def get_product_price(self):
        return self.browser.find_element(*self.PRODUCT_PRICE).text

    def should_display_success_message_with_correct_product_name(self):
        product_name = self.get_product_name()
        assert product_name in self.browser.find_element(*self.SUCCESS_MESSAGE).text, "Success message does not contain correct product name"

    def should_display_basket_total_with_correct_price(self):
        product_price = self.get_product_price()
        assert product_price in self.browser.find_element(*self.BASKET_TOTAL_MESSAGE).text, "Basket total message does not contain correct price"
