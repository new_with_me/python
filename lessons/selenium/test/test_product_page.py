# test_product_page.py

from product_page import ProductPage
from selenium.common.exceptions import NoAlertPresentException
import math
import pytest

@pytest.mark.parametrize('promo_offer', ["offer0", "offer1", "offer2", "offer3", "offer4", "offer5", "offer6", "offer7", "offer8", "offer9"])
def test_guest_can_add_product_to_basket(browser, promo_offer):
    product_page = ProductPage(browser, f"http://selenium1py.pythonanywhere.com/catalogue/the-shellcoders-handbook_209/?promo={promo_offer}")
    product_page.open()
    product_page.add_product_to_basket()
    product_page.solve_quiz_and_get_code()
    product_page.should_display_success_message_with_correct_product_name()
    product_page.should_display_basket_total_with_correct_price()
