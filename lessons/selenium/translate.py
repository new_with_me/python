from selenium import webdriver
from selenium.webdriver.common.by import By
import time


def tranlate(from_lang: str, to_lang: str, text: str) -> str:
    driver = webdriver.ChromeOptions()
    driver.add_argument("--headless")
    browser = webdriver.Chrome(options=driver)
    browser.get(f"https://translate.google.com/?hl={from_lang}&sl=en&tl={to_lang}&text={text}&op=translate")
    time.sleep(3)
    result = browser.find_element(By.CLASS_NAME, "KkbLmb").text
    res = result.split("\n")
    browser.close()
    return res[0]


print(tranlate("ru", "en", "Сегодня стояла хорошая погода"))
