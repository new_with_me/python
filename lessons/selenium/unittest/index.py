from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import pytest
import time
import math

class TestMainPage():

    @pytest.fixture(scope="function")
    def browser(self):
        print("\nstart browser for test..")
        browser = webdriver.Edge()
        yield browser
        print("\nquit browser for test..")
        browser.quit()

    def test_answer(self):
        fun = math.log(int(time.time()))
        browser = webdriver.Edge()
        browser.get("https://stepik.org/lesson/236897/step/1")
        input1 = browser.find_element(By.ID, "ember120")
        input1.send_keys(str(fun))
        browser.implicitly_wait(5)
        button = browser.find_element(By.CLASS_NAME, "submit-submission")
        button.click()
        time.sleep(5)
        browser.quit()

