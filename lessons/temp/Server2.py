import socketio
import uvicorn
from fastapi import FastAPI

app = FastAPI()
sio = socketio.AsyncServer(async_mode='asgi')
socket_app = socketio.ASGIApp(sio, app)

# создаем счетсик пользователей
storage = {"user_counter": 0}
users = []
@sio.event
async def connect(sid, environ):
		# меняем счетчик пользователей
    storage["user_counter"] += 1
    users.append(sid)
    print(f"Пользователь {sid} подключился")

@sio.event
async def disconnect(sid):
    storage["user_counter"] -= 1
    users.remove(sid)
    print(f"Пользователь {sid} отключился")
@app.get("/")
async def get_index():
    # отдаем счетчик пользователей
    return {"user_counter": storage["user_counter"], "users": users}

uvicorn.run(socket_app, host='0.0.0.0', port=8000)