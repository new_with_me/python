import datetime

import socketio
import eventlet

# Создаем экземпляр сервера Socket.IO
sio = socketio.Server(cors_allowed_origins='*')

# Создаем WSGI приложение
app = socketio.WSGIApp(sio)
users = {}


@sio.on('connect')
def connect(sid, environ):
    start = datetime.datetime.now().minute
    users[sid] = start
    print(f"Клиент {sid} подключился")
    user_status()
    users_all = len(users)
    sio.emit("hello", skip_sid=sid, data={"message": f"Сейчас в сети {users_all} пользователей"})
    sio.emit("hello", to=sid, data={"message": "Hello User"})
    print("connect ", sid)


@sio.on('disconnect')
def disconnect(sid):
    start = users[sid]
    del users[sid]
    stop = datetime.datetime.now().minute
    print(f"Клиент {sid} отключился через {stop - start} минут")
    user_status()
    sio.emit("off", to=sid,
             data={"message": f"Пользователь {sid} отключился. Сейчас в сети {len(users)} пользователей"})
    sio.emit("off", skip_sid=sid,
             data={"message": f"Пользователь {sid} отключился. Сейчас в сети {len(users)} пользователей"})


@sio.on("message")
def incoming_message(sid, data):
    sio.emit("message", to=sid, data={"message": data})


@sio.on('*')
def catch_all(event, sid, data):
    print(f"Received event '{event}' from client {sid} with data: {data}")
    sio.emit("error", {"message": f"No handler for event {event}"}, room=sid)


def user_status():
    if len(users) == 1:
        print("Пользователь один")
    elif len(users) > 1:
        print("Команда в сборе")
    else:
        print("Нет пользователей")


eventlet.wsgi.server(eventlet.listen(('192.168.50.218', 8000)), app)
