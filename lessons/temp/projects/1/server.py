import eventlet
from flask import Flask, render_template
import socketio

sio = socketio.Server()

# Создаем экземпляр Flask приложения
flask_app = Flask(__name__, template_folder="templates", static_folder="static")
app = socketio.WSGIApp(sio, flask_app)


@sio.event
def connect(sid, environ):
    print(f"Пользователь {sid} подключился")


@sio.event
def disconnect(sid):
    print(f"Пользователь {sid} отключился")


@flask_app.route("/")
def page_index():
    return render_template("index.html", name="Lariska", score=0)


if __name__ == "__main__":
    # flask_app.run(host='0.0.0.0', port=8000)
    eventlet.wsgi.server(eventlet.listen(('0.0.0.0', 8000)), app)
