Обработка подключения
Используйте декоратор sio.event и функцию connect для обработки события подключения.

sid - это идентификатор клиента, а environ- информация о соединении

    import socketio
    import eventlet
    
    # Создаем экземпляр сервера Socket.IO
    sio = socketio.Server()
    
    # Создаем WSGI приложение и связываем его с Socket.IO
    app = socketio.WSGIApp(sio)
    
    # Обработчик события подключения
    @sio.event
    def connect(sid, environ):
        print(f"Клиент {sid} подключен")
    
    # Обработчик события отключения
    @sio.event
    def disconnect(sid):
        print(f"Клиент {sid} отключен")
    
    # Запускаем Eventlet WSGI сервер
    if __name__ == '__main__':
        eventlet.wsgi.server(eventlet.listen(('0.0.0.0', 80)), app)

## Синхронные сервер: Werkzeug

Werkzeug - это средство разработки веб-приложений на языке Python, знакомый вам, если вы разрабатывали с помощью Flask. Он предоставляет общие функции и инструменты, которые помогают в создании веб-приложений.

    import socketio
    from werkzeug.serving import run_simple
    
    # Создаем WSGI приложение и связываем его с Socket.IO
    sio = socketio.Server()
    app = socketio.WSGIApp(sio)
    
    # Обработчик события подключения
    @sio.event
    def connect(sid, environ):
        print(f"Клиент {sid} подключен")
    
    # Обработчик события отключения
    @sio.event
    def disconnect(sid):
        print(f"Клиент {sid} отключен")
    
    # Запускаем WSGI сервер
    if __name__ == '__main__':
        run_simple('0.0.0.0', 80, app, use_reloader=True, use_debugger=True)

## Uvicorn

    import socketio
    import uvicorn
    
    # Создаем экземпляр асинхронного сервера Socket.IO
    sio = socketio.AsyncServer(async_mode='asgi')
    
    # Создаем ASGI приложение и связываем его с Socket.IO
    app = socketio.ASGIApp(sio)
    
    # Обработчик события подключения
    @sio.event
    async def connect(sid, environ):
        print(f"Клиент {sid} подключен")
    
    # Обработчик события отключения
    @sio.event
    async def disconnect(sid):
        print(f"Клиент {sid} отключен")
    
    # Запускаем сервер с помощью Uvicorn
    if __name__ == '__main__':
        uvicorn.run(app, host='0.0.0.0', port=80)

## Aiohttp

    import socketio
    from aiohttp import web
    
    # Создаем экземпляр сервера Socket.IO
    sio = socketio.AsyncServer(async_mode='aiohttp')
    
    # Создаем aiohttp веб-приложение и связываем его с Socket.IO
    app = web.Application()
    sio.attach(app)
    
    # Обработчик события подключения
    @sio.event
    async def connect(sid, environ):
        print(f"Клиент {sid} подключен")
    
    # Обработчик события отключения
    @sio.event
    def disconnect(sid):
        print(f"Клиент {sid} отключен")
    
    # Запускаем aiohttp веб-сервер
    if __name__ == '__main__':
        web.run_app(app, host='0.0.0.0', port=80)

## Фласк - синхронный микрофреймворк , который прекрасно подойдет для небольших приложений, которые работают в связке с сокетами. Давайте напишем небольшой пример, как они могут работать вместе:

    import eventlet
    from flask import Flask
    import socketio
    
    sio = socketio.Server()
    
    # Создаем экземпляр Flask приложения
    flask_app = Flask(__name__)
    
    app = socketio.WSGIApp(sio, flask_app)
    
    @sio.event
    def connect(sid, environ):
        print(f"Пользователь {sid} подключился")
    
    @sio.event
    def disconnect(sid):
        print(f"Пользователь {sid} отключился")
    
    @flask_app.route("/")
    def page_index():
        return "It works"
    
    eventlet.wsgi.server(eventlet.listen(('0.0.0.0', 80)), app)

## Fast API

Если наше приложение асинхронное, то нам подойдет Fast Api для создания хттп-эндпоинтов. Вот как будет выглядеть пример выше. Не забудьте установить зависимсти!

    import socketio
    import uvicorn
    from fastapi import FastAPI
    
    # Создаём экземпляр FastAPI приложения
    app = FastAPI()
    
    # Создаём экземпляр сервера Socket.IO с поддержкой асинхронного режима
    sio = socketio.AsyncServer(async_mode='asgi')
    # Оборачиваем FastAPI в Socket.IO ASGI приложение
    socket_app = socketio.ASGIApp(sio, app)
    
    @sio.event
    async def connect(sid, environ):
        print(f"Пользователь {sid} подключился")
    
    @sio.event
    async def disconnect(sid):
        print(f"Пользователь {sid} отключился")
    
    @app.get("/")
    async def get_index():
        return "it works"
    
    uvicorn.run(socket_app, host='0.0.0.0', port=80)
Сочетание HTTP и WebSocket позволяет обрабатывать как запросы через HTTP, так и отдавать статику, так и устанавливать постоянные соединения и обмениваться данными в режиме реального времени через WebSocket.

Подключение статики:

    static_files = { '/': 'static/index.html' }
    app = socketio.WSGIApp(sio, static_files=static_files)

Подключение Flask:

    flask_app = Flask(__name__)
    app = socketio.WSGIApp(sio, flask_app)

Подключение Fast API:

    app = FastAPI()
    socket_app = socketio.ASGIApp(sio, app)

