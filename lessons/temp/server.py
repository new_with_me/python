import socketio
import eventlet

# Создаем экземпляр сервера Socket.IO
sio = socketio.Server(cors_allowed_origins='*')

# Создаем WSGI приложение
app = socketio.WSGIApp(sio)

users = []
messages = {}
events_count = {}

scores = {}

@sio.on('connect')
def connect(sid, environ):
    sio.emit("hello", to=sid, data={"message": "hello socket"})
    users.append(sid)
    print("connect ", sid)


@sio.on('users_online')
def users_online(sid, data):
    sio.emit("users_online", to=sid, data={"users": users})
    print("users_online ", sid)


@sio.on('disconnect')
def disconnect(sid):
    users.remove(sid)
    print("disconnect ", sid)


@sio.on("message")
def incoming_message(sid, data):
    print("message", data, sid, messages[sid])
    sio.emit("message", to=sid, data={"message": data})


@sio.on("count_queries")
def count_queries(sid, data):
    print("count_queries", data, sid, events_count[sid])
    sio.emit("count_queries", to=sid, data={"message": events_count[sid]})


@sio.on('*')
def catch_all(event, sid, data):
    print(f"Received event '{event}' from client {sid} with data: {data}")
    sio.emit("error", {"message": f"No handler for event {event}"}, room=sid)
    events_count[sid] = events_count.get(sid, 0) + 1


@sio.on('increase')
def increase(sid, data):
    if sid in scores:
        scores[sid] += 1
    else:
        scores[sid] = 1
    print(scores[sid])
    sio.emit("increase", to=sid, data={"message": scores[sid]}, room=sid)
    print("increase", data, sid, "score[sid]")



@sio.on('decrease')
def decrease(sid, data):
    if sid in scores:
        scores[sid] -= 1
    else:
        scores[sid] = -1
    print(scores[sid])
    sio.emit("decrease", to=sid, data={"message": scores[sid]}, room=sid)

@sio.on('score')
def score(sid, data):
    sio.emit("score", to=sid, data={"message": scores[sid]})

eventlet.wsgi.server(eventlet.listen(('192.168.50.218', 8000)), app)

