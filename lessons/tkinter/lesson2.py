from PIL import Image, ImageTk
from tkinter import Tk, Label, BOTH, Frame


class Example(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
        self.create_menu()

    def create_menu(self):
        self.pack(fill=BOTH, expand=1)

        tiger = Image.open("1.jpeg")
        tigrula = ImageTk.PhotoImage(tiger)
        label1 = Label(self, image=tigrula)
        label1.image = tigrula
        label1.place(x=50, y=40)

        medved = Image.open("2.jpeg")
        vinipuh = ImageTk.PhotoImage(medved)
        label2 = Label(self, image=vinipuh)
        label2.image = vinipuh
        label2.place(x=50, y=300)

        telefon = Image.open("3.jpeg")
        tel = ImageTk.PhotoImage(telefon)
        label3 = Label(self, image=tel)
        label3.image = tel
        label3.place(x=450, y=40)

        cat = Image.open("1.jpeg")
        cat1 = ImageTk.PhotoImage(cat)
        label3 = Label(self, image=cat1)
        label3.image = cat1
        label3.place(x=450, y=300)


if __name__ == '__main__':
    root = Tk()
    root.title("Пример 3 - Абсолютное позиционирование в Tkinter")
    root.geometry("800x500+250+100")
    app = Example(root)
    root.mainloop()