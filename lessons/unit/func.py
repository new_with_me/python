import unittest


class MyTestCase(unittest.TestCase):

    def test_something(self):
        n = "{{[()]]"
        n=list(n)
        a: str = ']'
        b: str = ')'
        c: str = '}'
        a1: str = '['
        b1: str = '('
        c1: str = '{'
        for res,m in self.get_num(n):
            if(res == len(n)):
                break
            else:
                for j in range(0, res+1):
                    if n[j] == m:
                        n[j] = "0"
                        n[res] = "0"
                        break
        for el in range(0, len(n)):
            if n[el] == a:
                print(el+1)
                break
            if n[el] == b:
                print(el + 1)
                break
            if n[el] == c:
                print(el + 1)
                break

    def get_num(self, n: [str]) -> int:
        a: str = ']'
        b: str = ')'
        c: str = '}'
        a1: str = '['
        b1: str = '('
        c1: str = '{'
        for i in range(len(n)):
            if n[i] == a:
                yield i, a1
            if n[i] == b:
                yield i, b1
            if n[i] == c:
                yield i, c1

if __name__ == '__main__':
    unittest.main()
