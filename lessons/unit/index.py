import datetime
import unittest
import unittest.mock

def sum_two_values(x, y):
    return x + y


class UserTestCase(unittest.TestCase):
    def test_sum_two_values(self):
        with unittest.mock.patch('datetime.datetime') as mocked_data:
            mocked_data.now.return_value = (2019, 1, 1, 23, 8, 6)
            now = datetime.datetime.now()
            print(now)
            self.assertNotEquals(now, mocked_data.now())

