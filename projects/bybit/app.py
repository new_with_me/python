import socketio
import eventlet
from flask import Flask, render_template

sio = socketio.Server(cors_allowed_origins='*')
flask_app = Flask(__name__)
flask_app.template_folder = 'templates'
flask_app.static_folder = 'static'
app = socketio.WSGIApp(sio, flask_app)

template = 'templates'


@flask_app.route('/')
def index():
    return render_template('index.html')


@sio.event
def connect(sid, environ):
    print(f"Пользователь {sid} подключился")


@sio.event
def disconnect(sid):
    print(f"Пользователь {sid} отключился")


@sio.on("message")
def incoming_message(sid, data):
    sio.emit("message", to=sid, data={"message": data})


@sio.on("query")
def incoming_query(sid, data):
    sio.emit("query", skip_sid=sid, data={"message": data})


if __name__ == '__main__':
    eventlet.wsgi.server(eventlet.listen(('0.0.0.0', 5000)), app)
